void main() {
  final List<String> winApps = <String>[
    "FNB Banking App",
    "Snapscan",
    "LIVE Inspect",
    "WumDrop",
    "iKhoka.com",
    "Shyft",
    "Khula Ecosystem",
    "Naked",
    "EasyEquities"
        "Ambani Africa App"
  ];
  winApps.sort();
  print(winApps);

  var year1 = winApps.elementAt(5);
  var year2 = winApps.elementAt(6);

  print(
      "2017's App of the year winner was $year1 and 2018's App of the year winner was $year2");
}
